﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidzyDbFirst
{
	public class Program
	{
		static void Main(string[] args)
		{
			VidzyDbContext context = new VidzyDbContext();
			context.AddVideo("Hello", DateTime.Today, "Action", (byte)Classification.Gold);
		}
	}
}
